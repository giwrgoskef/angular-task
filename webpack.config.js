var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : null,
  entry: ["./src/js/app.js", "./src/js/controllers/AllControllers.js"],
  watch: true,
  module: {
	loaders: [
		{
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		loader: "babel-loader",
		query: {
			presets: ["es2015"],
			plugins: ["transform-class-properties", "transform-decorators-legacy"]
			}
		},
		{ 
		test: /\.json$/, 
		loader: 'json-loader' 
		},
		{
        test: /\.css$/,
		loader: "css-loader"
		},
		{
		test: /\.css$/,
		loader: "css-loader!autoprefixer-loader"
		},
		{
		test: /\.scss$/,
		loader: "css-loader!sass-loader"
		}
	]
  },
  output: {
    path: './src/bin/',
    filename: 'bundle.min.js'
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
};