var _ = require('underscore');

var app = angular.module('app');
app.controller('RepoController', ['$scope', '$routeParams', 
    'repositoryStoredData', function ($scope, $routeParams, repositoryStoredData) {
        "use strict";
        
        //creating filters
        $scope.reverseSort = true;
        $scope.orderByField = 'contributed';
        
        var currentRepo = $routeParams.repo;
        $scope.repoTitle = currentRepo;
        var orgRepos = repositoryStoredData.getOrgRepos();



        //if the length of orgRepos  = 0 or if orgRepos = undefined, then just display the header
        //thats to cover the case when the user reloads the user page
        if ((orgRepos.length === 0 || typeof orgRepos === 'undefined')) {
            $scope.isDataAvailable = false;
            $scope.errorLabel = "Sorry, the repository contributor data could not be loaded. \n\
Please use the Dashboard first to view a repository.";

        } else {
            $scope.isDataAvailable = true;
            $scope.contributionHeading = "Contributed To Repository";
            $scope.contributorHeading = "Contributions";

            var indexOfCurrentRepo = repositoryStoredData.indexOfRepo(currentRepo);

            var scopeData = [];


            var obj = {};

            var contributors = orgRepos[indexOfCurrentRepo]['contributors'];

            for (var j = 0; j < contributors.length; j++) {
                var contributees = contributors[j];


                obj = {
                    userName: contributees.login,
                    contributed: contributees.totalContribution,
                    avatar: contributees.avatar
                };
                scopeData.push(obj);
            }

            

            //using underscore sortby function
            scopeData = _.sortBy(scopeData, 'contributed');
            scopeData = scopeData.reverse();
            $scope.contributorData = scopeData;

            $scope.repoDesc = orgRepos[indexOfCurrentRepo].repoDesc;
            $scope.createdLabel = "Created at: ";
            $scope.repoCreated = orgRepos[indexOfCurrentRepo].repoCreated;
            $scope.updatedLabel = "Updated at: ";
            $scope.repoUpdated = orgRepos[indexOfCurrentRepo].repoUpdated;
            $scope.repoHtml = orgRepos[indexOfCurrentRepo].repoHtml;
            $scope.repoForksLabel = "Forks: ";
            $scope.forks = orgRepos[indexOfCurrentRepo].repoFork;
            $scope.repoStargazersLabel = "Stargazers: ";
            $scope.stargazers = orgRepos[indexOfCurrentRepo].repoStargazers;
            $scope.watchers = orgRepos[indexOfCurrentRepo].repoWatchers;
            $scope.repoWatchersLabel = "Watchers: ";

        }


    }]);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                .when('/users/:user', {
                    templateUrl: 'templates/profile.html',
                    controller: 'UserController'
                })
                .otherwise({
                    redirectTo: '/404',
                    templateUrl: 'templates/404.html'
                });
    }]);
