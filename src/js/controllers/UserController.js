var _ = require('underscore');

var app = angular.module('app');
app.controller('UserController', ['$scope', '$q', '$routeParams', 'githubWorker',
    'repositoryStoredData', function ($scope, $q, $routeParams, githubWorker,
            repositoryStoredData) {
        "use strict";
        //creating filters
        $scope.reverseSort = true;
        $scope.orderByField = 'contributed';
        
        var currentUser = $routeParams.user;

        $q.all([githubWorker.loadUserDetails(currentUser)])
                .then(function () {
                    var data = githubWorker.getUserDetails();

                    $scope.userTitle = data.name;
                    $scope.userLogin = currentUser;
                    $scope.userCompany = data.company;
                    $scope.userDesc = data.desc;
                    $scope.userBio = data.bio;
                    $scope.userBlog = data.blog;
                    $scope.linkLabel = "Github User Profile:";
                    $scope.userHtml = data.html_url;
                    $scope.reposNumberLabel = "Public Repos:";
                    $scope.pubRepos = data.pubRepos;
                    $scope.pubGists = data.pubGists;
                    $scope.gistsNumberLabel = "Public Gists:";
                        
                    var orgRepos = repositoryStoredData.getOrgRepos();

                    
                    if ((orgRepos.length === 0 || typeof orgRepos === 'undefined')) {
                        $scope.isDataAvailable = false;
                        $scope.errorLabel = "Sorry, the user contributions data could not be loaded. \n\
Please use the Dashboard first to view a profile.";

                    } else {
                        $scope.isDataAvailable = true;
                        
                        $scope.repoHeading = "Contributed To Repository";
                        $scope.contributionsHeading = "Contributions";

                        var scopeData = [];

                        for (var i = 0; i < orgRepos.length; i++) {
                            var obj = {};
                            var contributors = orgRepos[i]['contributors'];
                            for (var j = 0; j < contributors.length; j++) {
                                var contributees = contributors[j];

                                if (contributees.login === currentUser) {

                                    obj = {
                                        contributed: contributees.totalContribution,
                                        login: contributees.login,
                                        avatar: contributees.avatar,
                                        repoName: orgRepos[i].repoName,
                                        repoDesc: orgRepos[i].repoDesc
                                    };
                                    j = contributors.length;

                                }

                            }

                            if (_.isEmpty(obj)) {
                                obj = {};

                            } else {
                                scopeData.push(obj);
                            }

                        }
                        //using underscore sortby function
                        scopeData = _.sortBy(scopeData, 'contributed');
                        scopeData = scopeData.reverse();
                        $scope.contributionData = scopeData;
                    }
                });


    }]);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                .when('/repos/:repo', {
                    templateUrl: 'templates/repo.html',
                    controller: 'RepoController'
                })
                .otherwise({
                    redirectTo: '/404',
                    templateUrl: 'templates/404.html'
                });
    }]);
