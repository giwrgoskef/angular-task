var _ = require('underscore');

var app = angular.module('app');
app.controller('DashController', ['$scope', '$q',
    '$routeParams', 'githubWorker', 'repositoryStoredData',
    function ($scope, $q, $routeParams, githubWorker, repositoryStoredData) {
"use strict";

        //creating filters
        $scope.reverseSort = true;
        $scope.orderByField = 'contribution';
        
        if (typeof $routeParams.page === 'undefined') {
            $routeParams.page = 1;
        }
        var currentUser = "angular";

        var org = [];

        var highestContribs = [];
        var repos = [];
        var contribs = [];

        
        $scope.isDataAvailable = false;
      
        $q.all([
            githubWorker.loadUserDetails(currentUser),
            githubWorker.loadOrgRepos(currentUser)
        ]).then(function () {
            org = githubWorker.getUserDetails();
            repos = githubWorker.getPaginatedRepos($routeParams.page);
            $scope.userData = org;

            githubWorker.loadContributors(currentUser).then(function () {
                contribs = githubWorker.getContributors();

                githubWorker.loadContributorProfiles().then(function () {
                    highestContribs = githubWorker.getContributorProfiles();

                    var currentIndexOrgRepos = repositoryStoredData.getStartingIndex();
                    var maxItems = repositoryStoredData.getMaxLength();
                    var standardOutput = [];
                    var scopeData = [];


                    /* 
                     * Creating an standard object, setting it on service's orgRepos
                     */
                    for (var i = 0; i < repos.length; i++) {
                        //getting a ranking of repositories.  
                        var standardObject = {
                            repoName: repos[i].repoName,
                            repoDesc: repos[i].repoDesc,
                            repoHtml: repos[i].repoHtml,
                            repoCreated: repos[i].repoCreated,
                            repoUpdated: repos[i].repoUpdated,
                            repoStargazers: repos[i].repoStargazers,
                            repoWatchers: repos[i].repoWatchers,
                            repoFork: repos[i].repoFork,
                            contributors: contribs[i],
                            highestContributor: highestContribs[i]
                        };
                        //also creating an object to display
                        var scopeObj = {
                            repoName: repos[i].repoName,
                            repoDesc: repos[i].repoDesc,
                            contribution: highestContribs[i].totalContribution,
                            contributorName: highestContribs[i].name,
                            contributorLogin: highestContribs[i].login,
                            contribFollowers: highestContribs[i].followers,
                            contribRepos: highestContribs[i].pubRepos,
                            contribGists: highestContribs[i].pubGists

                        };
                        scopeData.push(scopeObj);
                        standardOutput.push(standardObject);
                        repositoryStoredData.addToOrgRepos(standardObject);
                    }
                    repositoryStoredData.setStandardOutput(standardOutput);
                    //using underscore sortby function
                    scopeData = _.sortBy(scopeData, 'contribution');
                    scopeData = scopeData.reverse();
                    
                    //scope variables
                    $scope.isDataAvailable = true;
                    $scope.rankingHeading = "#";
                    $scope.peopleHeading = "Contributor";
                    $scope.contributionHeading = "Contribution";
                    $scope.repoNameHeading = "Repository Name";
                    $scope.followersHeading = "Followers";
                    $scope.pubReposHeading = "Public Repos";
                    $scope.pubGistsHeading = "Public Gists";
                    $scope.mixedData = scopeData;
                    $scope.pagePrevious = parseInt($routeParams.page) - 1;
                    $scope.pageNext = parseInt($routeParams.page) + 1;
                    $scope.maxPages = repositoryStoredData.getTotalItems() / repositoryStoredData.getItems();

                });
            });
        });





    }]);
app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                .when('/users/:user', {
                    templateUrl: 'templates/profile.html',
                    controller: 'UserController'
                })
                .when('/repos/:repo', {
                    templateUrl: 'templates/repo.html',
                    controller: 'RepoController'
                })
                .when('/page:page', {
                    templateUrl: 'templates/dashboard.html',
                    controller: 'DashController'
                })
                .otherwise({
                    redirectTo: '/404',
                    templateUrl: 'templates/404.html'
                });
    }]);


var github = require('github-api');

app.factory('githubWorker', function ($q, repositoryStoredData, requestGitXML) {

    "use strict";

    function loadUserDetails(currentUser)
    {

        return requestGitXML.getGithubUser(currentUser)
                .then(function (callback) {
                    var data = callback.data;

                    requestGitXML.setOrgUserDetails(data);
                });
    }
    function loadOrgRepos(currentUser)
    {
        return requestGitXML.getOrganization(currentUser)
                .then(function (callback) {

                    var repoData = callback.data;

                    requestGitXML.setOrgRepos(repoData);
                });


    }

    function loadContributors(currentUser) {
        var repoData = repositoryStoredData.getRepos();
        var arrayLength = repoData.length;
        var contributionLinksCompleted = 0;
        var defer = $q.defer();
        var promises = [];

        for (var i = 0; i < arrayLength; i++) {
            var repoName = repoData[i].repoName;
            promises.push(requestGitXML.getGithubContributors(currentUser, repoName));
        }


        $q.all(promises).then(function (results) {
            var resultsArray = [];
            for (var i = 0; i < results.length; i++) {
                var rawData = results[i].data;

                if (rawData === null) {
                    //an empty repository was found
                    rawData = [];
                    var nullObj = {
                        author: {
                            login: "giwrgoskef",
                            avatar_url: "none"
                        },
                        total: 0
                    };
                    rawData.push(nullObj);
                }
                resultsArray.push(rawData);
                contributionLinksCompleted++;
            }

            if (contributionLinksCompleted === arrayLength) {
                requestGitXML.setContributors(resultsArray);
                defer.resolve();
            }
        });
        return defer.promise;
    }
    function loadContributorProfiles() {
        var contribData = repositoryStoredData.getContributors();
        var contributionLinksCompleted = 0;
        var defer = $q.defer();
        var promises = [];
        var arrayLength = contribData.length;

        for (var i = 0; i < arrayLength; i++) {
            //the json that is returned by the xml http request is sorted
            //by lowest contributors in each repository, so im getting the last.
            var last = contribData[i].length - 1;
            promises.push(requestGitXML.getGithubUser(contribData[i][last].login));
        }
        $q.all(promises).then(function (results) {
            var resultsArray = [];
            for (var i = 0; i < results.length; i++) {
                var rawData = results[i].data;

                resultsArray.push(rawData);
                contributionLinksCompleted++;
            }


            if (contributionLinksCompleted === arrayLength) {
                requestGitXML.setContributorProfiles(resultsArray);
                defer.resolve();
            }
        });
        return defer.promise;
    }

    return{
        loadUserDetails: loadUserDetails,
        loadOrgRepos: loadOrgRepos,
        loadContributors: loadContributors,
        loadContributorProfiles: loadContributorProfiles,
        getUserDetails: function () {
            var data = requestGitXML.getOrgUserDetails();
            var userData = {
                login: data.login,
                avatar: data.avatar_url,
                name: data.name,
                desc: data.description,
                company: data.company,
                bio: data.bio,
                blog: data.blog,
                followers: data.followers,
                pubRepos: data.public_repos,
                pubGists: data.public_gists,
                html_url: data.html_url
            };
            repositoryStoredData.setOrganization(userData);
            return userData;
        },
        getPaginatedRepos: function (route) {
            var repoData = requestGitXML.getOrgRepos();
            var pageNumber = route;
            var items = 25;
            //the max length property is linked with the number of items displayed on the page
            if (pageNumber * items <= repoData.length) {
                var length = pageNumber * items;
            } else {
                var length = repoData.length;
            }
            repositoryStoredData.setMaxLength(length);
            repositoryStoredData.setTotalItems(repoData.length);
            repositoryStoredData.setItems(items);
            var startingIndex = (pageNumber * items) - items;
            repositoryStoredData.setStartingIndex(startingIndex);
            var repos = [];
            
            for (var i = startingIndex; i < length; i++) {

                var obj = {
                    repoName: repoData[i].name,
                    repoDesc: repoData[i].description,
                    repoHtml: repoData[i].html_url,
                    repoCreated: repoData[i].created_at,
                    repoUpdated: repoData[i].updated_at,
                    repoStargazers: repoData[i].stargazers_count,
                    repoWatchers: repoData[i].watchers_count,
                    repoFork: repoData[i].forks_count
                };
                repos.push(obj);
            }
            repositoryStoredData.setRepositories(repos);

            return repos;
        },
        getContributors: function () {
            var repoData = repositoryStoredData.getRepos();
            var arrayLength = repoData.length;
            var urls = [];
            var contribArray = [];
            for (var i = 0; i < arrayLength; i++) {
                urls.push(repoData[i].repoHtml);
            }

            var results = requestGitXML.getContributors();
            for (var i = 0; i < results.length; i++) {
                var rawData = results[i];
                var contribData = [];
                for (var j = 0; j < rawData.length; j++) {
                    var contribObj = {
                        login: rawData[j].author.login,
                        avatar: rawData[j].author.avatar_url,
                        url: rawData[j].author.url,
                        totalContribution: rawData[j].total,
                        repoContributedToUrl: urls[i]
                    };
                    contribData.push(contribObj);
                }

                contribArray.push(contribData);


            }
            repositoryStoredData.setContributors(contribArray);
            return contribArray;
        },
        getContributorProfiles: function () {
            var contribData = repositoryStoredData.getContributors();
            var results = requestGitXML.getContributorProfiles();
            var contribNamesArray = [];

            for (var i = 0; i < results.length; i++) {
                var profiles = contribData[i];
                var rawData = results[i];
                var last = profiles.length - 1;
                var totalContributionForLast = profiles[last].totalContribution;
                var userData = {
                    login: rawData.login,
                    avatar: rawData.avatar_url,
                    name: rawData.name,
                    desc: rawData.description,
                    blog: rawData.blog,
                    totalContribution: totalContributionForLast,
                    followers: rawData.followers,
                    pubRepos: rawData.public_repos,
                    pubGists: rawData.public_gists
                };

                contribNamesArray.push(userData);
            }
            repositoryStoredData.setHighestContrib(contribNamesArray);
            return contribNamesArray;
        }
    };
});
app.service('requestGitXML', function () {
    var tempOrgUserDetails = [];
    var tempOrgRepos = [];
    var tempContributors = [];
    var tempContributorProfiles = [];

    var gh = new github({
        token: 'eaccf3f5aa38edc57feaa99ade7d1fc4b393f37e'
    });
    return{
        getGithubUser: function (user) {
            var userDetails = gh.getUser(user);
            return userDetails.getProfile();
        },
        getOrganization: function (user) {
            var org = gh.getOrganization(user);
            return org.getRepos();
        },
        getGithubContributors: function (owner, repositoryName) {
            var repo = gh.getRepo(owner, repositoryName);
            return repo.getContributors();
        },
        getOrgUserDetails: function () {
            return tempOrgUserDetails;
        },
        setOrgUserDetails: function (value) {
            tempOrgUserDetails = value;
        },
        getOrgRepos: function () {
            return tempOrgRepos;
        },
        setOrgRepos: function (value) {
            tempOrgRepos = value;
        },
        getContributors: function () {
            return tempContributors;
        },
        setContributors: function (value) {
            tempContributors = value;
        },
        getContributorProfiles: function () {
            return tempContributorProfiles;
        },
        setContributorProfiles: function (value) {
            tempContributorProfiles = value;
        }

    };
});
/* This service stores repository data from the controller above and can share to every other.
 * The service has setter and getter functions to set and retrive values.
 * Each index of repositories and contributors array will contain an object.
 * Each index matches in all three arrays and an index represents a repository. 
 */
app.service('repositoryStoredData', function () {
    //this orgRepos contains all repositories
    var orgRepos = [];
    var orgReposCurrentIndex;
    var maxLength;
    var totalItems;
    var itemsInPage;
    var organization = [];
    var repositories = [];
    var contributors = [];
    var highestContributor = [];
    var standardOutput = [];

    /*
     * given an object, this method verifies if the value already exists in the current array
     * to avoid adding repetitions to the array that stores every repository
     * match the given value against each repository stored in orgRepos,
     * return false if finds an equal
     */
    function isAddingUniques(value) {
        for (var i = 0; i < orgRepos.length; i++) {
            var currentRepository = orgRepos[i];

            if (_.isEqual(value, currentRepository)) {
                return false;
            }
        }
        return true;

    }

    return{
        addToOrgRepos: function (value) {
            if (orgReposCurrentIndex !== maxLength && value.length !== 0) {
                if (isAddingUniques(value)) {
                    orgRepos.push(value);
                    orgReposCurrentIndex++;
                }
            }

        },
        getStartingIndex: function () {
            return orgReposCurrentIndex;
        },
        setStartingIndex: function (value) {
            orgReposCurrentIndex = value;
        },
        getTotalItems: function () {
            return totalItems;
        },
        setTotalItems: function (value) {
            totalItems = value;
        },
        getItems: function () {
            return itemsInPage;
        },
        setItems: function (value) {
            itemsInPage = value;
        },
        getOrganization: function () {
            return organization;
        },
        setOrganization: function (value) {
            organization = value;
        },
        getHighestContrib: function () {
            return highestContributor;
        },
        setHighestContrib: function (value) {
            highestContributor = value;
        },
        getRepos: function () {
            return repositories;
        },
        setRepositories: function (value) {
            repositories = value;
        },
        getContributors: function () {
            return contributors;
        },
        setContributors: function (value) {
            contributors = value;
        },
        getRepoNameAt: function (value) {
            return repositories[value].name;
        },
        getRepoDescAt: function (value) {
            return repositories[value].desc;
        },
        getMaxLength: function (value) {
            return maxLength;
        },
        setMaxLength: function (value) {
            maxLength = value;
        },
        getOrgRepos: function () {
            return orgRepos;
        },
        setOrgRepos: function (value) {
            orgRepos = value;
        },
        getStandardOutput: function () {
            return standardOutput;
        },
        setStandardOutput: function (value) {
            standardOutput = value;
        },
        /* 
         * given a value, search the repository descriptions for a match and returns its index
         * if not found, return -1.
         */
        indexOfRepo: function (value) {
            var repoLength = orgRepos.length;
            for (var i = 0; i < repoLength; i++) {
                if (value === orgRepos[i].repoName) {
                    return i;
                }
            }
            return -1;
        }

    };
});

