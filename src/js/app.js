/* 
 * This file app.js is part of the angularank project and  
 * was written and developed by Ronaldo Campos <infinitesimalsun at gmail.com> 
 * on Jul 13, 2016.
 */

/*This module only initializes the app and configure routes,
 * as well as initializing some variables to share with other 
 * controllers via service.
 */


var angular = require('angular');
require('angular-route');

var app = angular.module('app', ['ngRoute']);
app.config(['$routeProvider',
    function ($routerProvider) {
        $routerProvider

                .when('/', {
                    templateUrl: "templates/dashboard.html",
                    controller: "githubDashCtrl"
                })
                            
                .otherwise({
                    redirectTo: '/404',
                    templateUrl: 'templates/404.html'
                });

    }]);
